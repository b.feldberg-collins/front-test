const knex = require('./knex')

getCard = (id) => {
    return knex('cards').where('id', id).select('*')
}

getAllCards = (id) => {
    return knex('cards').select('*')
}

createCard = (card) => {
    return knex('cards').insert(card)
}

module.exports = {
    getCard,
    getAllCards,
    createCard,
}
