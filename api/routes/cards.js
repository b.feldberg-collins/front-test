const router = require('express').Router()
const db = require('../db/cards')

router.route('/:id').get((req, res) => {
    db.getCard(req.params.id)
        .then((data) => {
            if (data.length == 0) {
                res.status(400).json({ message: `Card with id: ${req.params.id} not found` })
            } else {
                res.status(200).json(data)
            }
        })
        .catch((err) => res.status(400).json(`Error: ${err}`))
})

router.route('/').get((req, res) => {
    db.getAllCards()
        .then((cards) => res.json(cards))
        .catch((err) => res.status(400).json(`Error: ${err}`))
})

router.route('/create').post(async (req, res) => {
    console.log(req.body)
    await db
        .createCard(req.body)
        .then((data) => res.status(201).json({ message: 'card created', data: data[0] }))
        .catch((err) => res.status(400).json(`Error: ${err}`))
})

module.exports = router
