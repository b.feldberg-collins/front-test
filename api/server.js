const express = require('express')
const cors = require('cors')
require('dotenv').config()

const app = express()
const port = process.env.PORT || 5000

app.use(cors())
app.use(express.json())

const cardsRouter = require('./routes/cards')

app.use('/cards', cardsRouter)

app.listen(port, () => {
    console.log(`app is running on port: ${port}`)
})