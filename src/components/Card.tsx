import dayjs from 'dayjs'
import React from 'react'
import Skeleton from 'react-loading-skeleton'
import { CardData } from '../types/Card'
import { CardBody, CardImage, CardImageContainer, StyledCard, Text } from './Card/Styles'

type CardType = {
    data?: CardData
}

export const Card: React.FC<CardType> = (props) => {
    const { data } = props

    return (
        <StyledCard>
            <CardImageContainer>
                {data ? (
                    <CardImage src={data.image} width="100%" loading="eager" />
                ) : (
                    <Skeleton height={150} width={150} />
                )}
            </CardImageContainer>
            <CardBody>
                <Text color={'#333'}>{data ? `First: ${data?.firstname}` : <Skeleton />}</Text>
                <Text color={'#333'}>{data ? `Last: ${data?.lastname}` : <Skeleton />}</Text>
                <Text color={'#333'}>
                    {data ? `DoB: ${dayjs(data.birthday).format('DD/MM/YYYY')}` : <Skeleton />}
                </Text>
            </CardBody>
        </StyledCard>
    )
}
