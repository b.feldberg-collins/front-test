import styled from 'styled-components'

const cardRadius = 4
const cardShadow = '1px 1px 4px 0px #31313161'

export const CardBody = styled.div`
    display: flex;
    flex-direction: column;
    background-color: #fff;
    border-radius: 0 0 ${cardRadius}px ${cardRadius}px;
    box-shadow: ${cardShadow};
    border-top: 0;
    padding: 20px 15px;
    margin: 0 6px;
    transition: 0.3s ease;
`

export const CardImageContainer = styled.div`
    border-radius: ${cardRadius}px ${cardRadius}px 0 0;
    box-shadow: ${cardShadow};
    z-index: 2;
    overflow: hidden;
    display: flex;
    transition: 0.3s ease;
`

export const CardImage = styled.img`
    width: 100%;
    height: 100%;
    object-fit: cover;
`
export const StyledCard = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
    transition: 0.3s ease;
    &:hover {
        transform: scale(1.1);
        ${CardBody}, ${CardImageContainer} {
            box-shadow: 3px 5px 4px 0px #31313161;
        }
    }
`

export const Text = styled.p`
  color: ${(props: any) => props.color}
  margin: 0;
  margin-bottom: 0.25rem;
  font-size: 16px;
  text-align: left;
`
