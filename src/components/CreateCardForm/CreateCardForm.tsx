import React, { useState } from 'react'
import { CardData } from '../../types/Card'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

import { fetchRequest } from '../../utils/fetch-request'
import { endpoints } from '../../endpoints'
import dayjs from 'dayjs'
import { toast } from 'react-toastify'
import styled from 'styled-components'

const initialValues = {
    image: 'https://pbs.twimg.com/media/E6WbTaBUUAYD_OD.jpg',
    firstname: '',
    lastname: '',
    birthday: new Date().toDateString(),
}

const StyledInput = styled.input`
    margin-bottom: 15px;
    margin-left: 15px;
`

const StyledLabel = styled.label`
    display: flex;
    justify-content: space-between;
`
export const CreateCardForm: React.FC<{
    setId: React.Dispatch<React.SetStateAction<number | undefined>>
}> = ({ setId }) => {
    const [values, setValues] = useState<CardData>(initialValues)

    const [submitting, setSubmitting] = useState(false)

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()

        setSubmitting(true)
        console.log(values)
        await fetchRequest({ url: endpoints.cards.create, data: values, method: 'post' })
            .then((res) => {
                toast.success(res.message)
                console.log(res.data)
                setId(res.data)
                setSubmitting(false)
                setValues(initialValues)
            })
            .catch((err) => setSubmitting(false))
    }
    return (
        <form
            onSubmit={(e) => handleSubmit(e)}
            style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                maxWidth: '400px',
            }}
        >
            <StyledLabel>
                First name:
                <StyledInput
                    type="text"
                    value={values.firstname}
                    onChange={(e) => setValues({ ...values, firstname: e.target.value })}
                    required
                />
            </StyledLabel>
            <StyledLabel>
                Last name:
                <StyledInput
                    type="text"
                    value={values.lastname}
                    onChange={(e) => setValues({ ...values, lastname: e.target.value })}
                    required
                />
            </StyledLabel>
            <StyledLabel
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    whiteSpace: 'nowrap',
                    marginBottom: '15px',
                }}
            >
                Date of Birth:
                <div style={{ marginLeft: '15px' }}>
                    <DatePicker
                        selected={dayjs(values.birthday).toDate()}
                        onChange={(date) =>
                            setValues({ ...values, birthday: date?.toString() || '' })
                        }
                    />
                </div>
            </StyledLabel>
            <input type="submit" value="Submit" disabled={submitting} />
        </form>
    )
}
