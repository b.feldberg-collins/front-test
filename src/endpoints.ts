const base_url = process.env.REACT_APP_API_URL

export const endpoints = {
    cards: {
        get: `${base_url}/cards`,
        getOne: (id: number) => `${base_url}/cards/${id}`,
        create: `${base_url}/cards/create`,
    },
}
