import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import { Collection } from './pages/Collection'
import { CreateCard } from './pages/CreateCard'

import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

const App = () => (
    <Router>
        <Switch>
            <Route exact path="/collection" component={Collection} />
            <Route exact path="/create-card" component={CreateCard} />
        </Switch>
        <ToastContainer />
    </Router>
)

render(<App />, document.getElementById('root'))
