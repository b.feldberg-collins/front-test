import { toast } from 'react-toastify'
import { endpoints } from '../endpoints'
import { CardData } from '../types/Card'
import { fetchRequest } from '../utils/fetch-request'

export function fetchCollection() {
    return fetchRequest<CardData[]>({ url: endpoints.cards.get })
        .then((res) => {
            return res.map((r) => {
                return { ...r, birthday: r.birthday.split('T')[0] }
            })
        })
        .catch((err) => {
            toast.error(err.message)
            return null
        })
}
