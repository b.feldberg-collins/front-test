import React, { useCallback, useEffect, useState } from 'react'
import styled from 'styled-components'
import { Card } from '../components/Card'
import { fetchCollection } from '../lib/collection'
import { CardData } from '../types/Card'

const CardGrid = styled.div`
    padding: 0 90px;
    display: grid;
    align-items: center;
    @media (min-width: 768px) {
        grid-template-columns: repeat(3, 1fr);
    }

    @media (min-width: 982px) {
        grid-template-columns: repeat(4, 1fr);
    }
    @media (max-width: 499px) {
        grid-template-columns: repeat(1, 1fr);
    }
    grid-column-gap: 30px;
    grid-template-rows: 1fr;
    grid-row-gap: 30px;
`

export const Collection: React.FC = () => {
    const [collection, setCollection] = useState<CardData[]>(Array(10).fill(null))

    const getCards = useCallback(async () => {
        await fetchCollection()
            .then((cards) => cards && setCollection(cards))
            .catch((err) => console.error(err.message))
    }, [])

    useEffect(() => {
        setTimeout(() => {
            getCards()
        }, 500)
    }, [])

    return (
        <CardGrid>
            {collection?.map((card, idx) => (
                <Card key={`card-${idx}`} data={card} />
            ))}
        </CardGrid>
    )
}
