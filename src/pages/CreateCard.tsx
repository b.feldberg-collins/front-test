import React, { useEffect, useState } from 'react'
import { Card } from '../components/Card'
import { CreateCardForm } from '../components/CreateCardForm/CreateCardForm'
import { endpoints } from '../endpoints'
import { CardData } from '../types/Card'
import { fetchRequest } from '../utils/fetch-request'

export const CreateCard = () => {
    const [id, setId] = useState<number>()
    const [newCard, setNewCard] = useState<CardData>()

    useEffect(() => {
        console.log(id)
        if (id)
            fetchRequest<CardData[]>({ url: endpoints.cards.getOne(id) }).then((data) =>
                setNewCard(data[0]),
            )
    }, [id])

    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100vh',
                flexDirection: 'column',
            }}
        >
            {newCard && (
                <div style={{ maxWidth: '200px', marginBottom: '30px' }}>
                    <Card data={newCard} />
                </div>
            )}
            <CreateCardForm setId={setId} />
        </div>
    )
}
