export type CardData = {
    id?: number
    image: string
    firstname: string
    lastname: string
    birthday: string
}
