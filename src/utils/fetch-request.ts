const authHeaders: HeadersInit = {
    Authorization: `Bearer ${localStorage.getItem('token')?.toString()}`,
}

type RequestType = {
    url: string
    method?: 'GET' | 'post' | 'PUT' | 'DELETE'
    data?: { [key: string]: any }
    authorized?: boolean
    headers?: { [key: string]: string }
}

export const fetchRequest = async <Type = any>(props: RequestType): Promise<Type> => {
    const { url, method = 'GET', data, authorized, headers } = props

    return await fetch(url, {
        method,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            ...(authorized && { ...authHeaders }),
            ...headers,
        },
        body: JSON.stringify(data),
    })
        .then(async (res) => {
            if (res.status >= 200 && res.status < 300) {
                return res.json()
            } else {
                const response = await res.json()
                const err = new Error(res.statusText)
                err.message = response.message
                return Promise.reject(err)
            }
        })
        .catch((err) => {
            console.error(err)
            throw new Error(err.message)
        })
}
